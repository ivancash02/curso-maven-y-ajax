package ejercicio_usuarios;

import java.util.Scanner;

public class Usuario {

	// VARIABLES MIEMBRO (propiedades, atributos, campos....)
	String nombre;
	String dni;
	String telefono;
	int edad;
	float salario;
	boolean esactivo;
	char sexo='h';

	public Usuario() {
		// this.sexo='h';
		this.dni= generaDni();
	}
	// Constructores; Son metodos especiales para la creacion instanciacion o
	// construccion de objetos
	// Java crea un constructor por defecto sin parámetros

	public Usuario(String nombre, String telefono, int edad, float salario, boolean esactivo, char sexo) {

		this.nombre = nombre;
		this.dni = generaDni();
		this.telefono = telefono;
		this.edad = edad;
		this.salario = salario;
		this.esactivo = esactivo;
		//DETERMINAMOS QUE EL SEXO POR DEFECTO ES HOMBRE
		if (sexo != 'm') {
			this.sexo = 'h';
		} else {
			this.sexo = 'm';
		}

	}

	// Funciones miembro, comunes a los objetos, pero propias de cada objeto porque
	// solo pueden acceder a sus campos.

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDni() {
		return dni;
	}

	

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public float getSalario() {
		return salario;
	}

	public void setSalario(float salario) {
		this.salario = salario;
	}

	public String isEsactivo() {
		String actividad="";
		
		if(this.esactivo) {
			actividad="Trabajando";
		}else {
			actividad="En paro";
		}
		
		return actividad;
	}

	public void setEsactivo(boolean esactivo) {
		this.esactivo = esactivo;
	}

	public String getSexo() {
		String sex="";
		if(this.sexo=='m') {
			sex="mujer";
		}else {
			sex="hombre";
		}
		return sex;
	}
//ESTE ES EL METODO QUE ESTABLECE EL SEXO MEDIANTE STRING... 
	public void setSexo(String sex) {

		if (Character.toLowerCase(sex.charAt(0)) == 'm') {
			this.sexo = 'm';

		} else {
			this.sexo = 'h';
		}

	}

	boolean mayorEdad() {
		/*
		 * if(edad>18) { return true; }else {
		 */
		return this.edad >= 18; // con esta sentencia evitamos la estructura en if.

	}

	void mostrarDatos() {
		System.out.println();
		System.out.print("Dni: " + this.dni + "\n" + "nombre: " + this.nombre + "\n" + "edad: " + this.edad + "\n"
				+ "sexo: " + getSexo()+"\n"+ getClass());
	

	}

	void aumentarSalario(float cantidad) {

		this.salario += cantidad;
	}
	private String generaDni() {
		String dni="";
		String numeroDni="";
		char[] letra= {'a','b','c','d','e'};
		
		int numero= (int)(Math.random()*100000000)+1;
		
		numeroDni= String.format("%08d", numero);
		
		
		
		int aleatorio=(int)(Math.random()*4)+1;
		
			for(int i=0;i<letra.length;i++) {
				dni= numeroDni+ String.valueOf(letra[aleatorio]);
			}
		
		
		return dni;
	}

	@Override
	public String toString() {
		return "Usuario [nombre=" + nombre + ", dni=" + dni + ", telefono=" + telefono + ", edad=" + edad + ", salario="
				+ salario + ", es activo=" + isEsactivo() + ", sexo=" + getSexo() + "]";
	}

}
