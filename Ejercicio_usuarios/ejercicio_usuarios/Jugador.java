package ejercicio_usuarios;

public class Jugador extends Usuario {
	
	private int nivel;
	private String nick;
	public Jugador() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Jugador(String nombre, String telefono, int edad, float salario, boolean esactivo, char sexo,int nivel, String nick) {
		super(nombre, telefono, edad, salario, esactivo, sexo);
		// TODO Auto-generated constructor stub
		this.nivel=nivel;
		this.nick=nick;
	}
	
	
	
	
	public int getNivel() {
		return nivel;
	}
	public void setNivel(int nivel) {
		this.nivel = nivel;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	@Override
	void mostrarDatos() {
		// TODO Auto-generated method stub
		super.mostrarDatos();
		System.out.print("\nnick: " + getNick()+"\n"+"level: "+ getNivel()+"\n"+ getClass());
		System.out.println("-------------------------------");
		System.out.println();
	}
	@Override
	public String toString() {
		return "Jugador [nivel=" + nivel + ", nick=" + nick + ", nombre=" + nombre + ", dni=" + dni + ", telefono="
				+ telefono + ", edad=" + edad + ", salario=" + salario + ", es activo=" + isEsactivo() + ", sexo=" + getSexo()
				+ "]";
	}
	
	

}
