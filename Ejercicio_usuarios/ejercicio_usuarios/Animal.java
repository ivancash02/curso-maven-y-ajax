package ejercicio_usuarios;

public class Animal {

	private String nombre;
	private String especie;
	private char sexo;
	private int edad;

	public Animal() {
		super();
	}

	public Animal(String nombre, String especie, char sexo, int edad) {
		super();
		this.nombre = nombre;
		this.especie = especie;
		if (sexo != 'h') {
			this.sexo = 'm';
		} else {
			this.sexo = sexo;
		}

		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEspecie() {
		return especie;
	}

	public void setEspecie(String especie) {
		this.especie = especie;
	}

	public String getSexo() {
		String sex="";
		
		if(this.sexo=='m') {
			sex="macho";
		}else {
			sex="hembra";
		}
		return sex;
	}

	public void setSexo(String sexx) {
		if(Character.toLowerCase(sexx.charAt(0)) == 'h') {
			this.sexo='h';
		}
		else {
			this.sexo = 'm';
		}
		
		
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	@Override
	public String toString() {
		return "Animal [nombre=" + nombre + ", especie=" + especie + ", sexo=" + getSexo() + ", edad=" + edad + "]";
	}
	
	

}
