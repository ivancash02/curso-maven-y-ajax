package ejercicio_usuarios;

import java.util.Date;
import java.util.GregorianCalendar;

public class Administrador extends Usuario {

	private String permisos;
	private Date fecha_alta;
	
	
	
	public Administrador() {
		super();
	}



	public Administrador(String permisos, int dia, int mes, int anio) {
		super();
		this.permisos = permisos;
		GregorianCalendar calendario= new GregorianCalendar(anio,mes-1,dia);
		
		this.fecha_alta = calendario.getTime();
	}



	public Administrador(String nombre, String telefono, int edad, float salario, boolean esactivo, char sexo,String permisos, int dia, int mes, int anio) {
		super(nombre, telefono, edad, salario, esactivo, sexo);
		// TODO Auto-generated constructor stub
		GregorianCalendar calendario= new GregorianCalendar(anio,mes-1,dia);
		this.permisos = permisos;
		this.fecha_alta = calendario.getTime();
	}



	public String getPermisos() {
		return permisos;
	}



	public void setPermisos(String permisos) {
		this.permisos = permisos;
	}



	public Date getFecha_alta() {
		return fecha_alta;
	}



	public void setFecha_alta(Date fecha_alta) {
		this.fecha_alta = fecha_alta;
	}



	@Override
	void mostrarDatos() {
		// TODO Auto-generated method stub
		super.mostrarDatos();
		System.out.print("\npermisos: " + getPermisos()+"\n"+"Fecha de Alta: "+ getFecha_alta()+"\n"+ getClass());
		System.out.println("-------------------------------");
		System.out.println();
	}



	@Override
	public String toString() {
		return "Administrador [permisos=" + permisos + ", fecha_alta=" + fecha_alta + ", nombre=" + nombre + ", dni="
				+ dni + ", telefono=" + telefono + ", edad=" + edad + ", salario=" + salario + ", es activo=" + isEsactivo()
				+ ", sexo=" + getSexo() + "]";
	} 
	
	
	
}
