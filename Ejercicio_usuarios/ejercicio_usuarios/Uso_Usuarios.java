package ejercicio_usuarios;

public class Uso_Usuarios {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Usuario user1 = new Usuario();
		Usuario[] usuarios = new Usuario[5];

		usuarios[0] = new Jugador("Ivan", "640771045", 42, 2500.00f, true, 'h', 25, "ivancash");
		usuarios[1] = new Usuario("paca medidna", "242342", 23, 2300.34f, true, 'm');
		usuarios[2] = user1;
		usuarios[3] = new Administrador("El jefe", "696969696", 55, 3500.5f, true, 'm', "TODOS LOS PERMISOS", 23, 3,
				1978);
		usuarios[4] = new Jugador();

		Animal[] animales = new Animal[2];

		animales[0] = new Animal("pancho", "perro", 'm', 3);
		animales[1] = new Animal("perico", "gato", 'f', 2);

		for (int i = 0; i < usuarios.length; i++) {
			System.out.println(usuarios[i].toString().toUpperCase());
			System.out.println();
		}

		for (int i = 0; i < animales.length; i++) {
			if(animales[i].getSexo().equalsIgnoreCase("macho")) {
				animales[i].setSexo("hembra");
			}
			System.out.println(animales[i].toString());
			
		}

	}

}
